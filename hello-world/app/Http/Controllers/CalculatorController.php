<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function index(Request $request){
    	$firstoperand = $request->post('firstoperand');
    	$secondoperand = $request->post('secondoperand');

    	$add = ($firstoperand + $secondoperand);
    	

    	return view('index',['result'=>'Sum: '.$add,'firstoperand'=>$firstoperand,'secondoperand'=>$secondoperand]);
    }
}
