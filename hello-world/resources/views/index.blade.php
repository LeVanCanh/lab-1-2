{{ Form::open(array('url' => 'calculator')) }}
<table align="center" border="1">
	<tr>
		<td>First Operand</td>
		<td>{{Form::text('firstoperand',$firstoperand ?? '')}}</td>
	</tr>
	<tr>
		<td>Second Operand</td>
		<td>{{Form::text('secondoperand',$secondoperand ?? '')}}</td>
	</tr>
	<tr>
		<td colspan="2" align="center">{{Form::submit('Click Me!')}}</td>
	</tr>
	<tr>
		<td colspan="2" align="center">{{$result ?? ''}}</td>
	</tr>
</table>
{{ Form::close() }}