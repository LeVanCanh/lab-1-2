<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hello', 'HelloController@index');

Route::get('/about', function (){
	echo "<h2>Day la trang about</h2>";
});
Route::get('/shop', function (){
	echo "<h2>Day la trang shop</h2>";
});
Route::get('/test', function () {
    return view('test',['name'=>'Le Van Canh']);
});

Route::get('student/{name}', function ($name) {
    echo 'Student Name is ' . $name;
});

Route::get('users/{name}', function ($name="Le Van Canh") {
    echo 'User Name is ' . $name;
});

Route::get('/', 'homeController@index');

Route::get('/calculator',function(){
	return view('index');
});

Route::post('/calculator','CalculatorController@index');



